package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class AccountData {
    public static List<Account> getAccountsList(){
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(1, 100));
        accounts.add(new Account(2, 200));
        accounts.add(new Account(3, -300));
        accounts.add(new Account(4, 400));

        return accounts;
    }
}
