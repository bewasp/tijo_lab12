package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification{

    @Unroll
    def "should deposit #moneyToDeposit money to account number #accountNumber"(){

        given: "initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)


        when: "deposit money to account"
        def score = bank.deposit(accountNumber, moneyToDeposit)

        then: "check if deposit is successful"
        score

        where:
        accountNumber | moneyToDeposit
        1       |       50
        2       |       500
    }
}
