package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "should withdraw #moneyToWithdraw of #money from account #accountNumber"(){

        given: "initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)

        when: "withdraw money"
        def score = bank.withdraw(accountNumber, moneyToWithdraw)

        then: "check if withdraw is successful"
        score

        where:
        accountNumber | money | moneyToWithdraw
        1       |  200 |      100
        2       |  100 |      50
    }
}
