package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountsBalanceSpec extends Specification{

    @Unroll
    def "should amount and return #amount from all accounts"(){

        given: "Initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)

        when: "amount balance from all accounts"
        def score = bank.sumAccountsBalance()

        then: "check amount of all accounts"
        score == amount

        where:
        amount | _
        200 | _
        150 | _
        390 | _
    }
}
