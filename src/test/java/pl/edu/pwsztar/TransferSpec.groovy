package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification{

    @Unroll
    def "should transfer from #fromAccount to #toAccount account #money money"(){

        given: "initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)

        when: "transfer money"
        def score = bank.transfer(fromAccount, toAccount, money)

        then: "check if transfer is successful"
        score

        where:
        fromAccount | toAccount | money
        3       |       1       | 300
        4       |       2       | 100
    }
}
