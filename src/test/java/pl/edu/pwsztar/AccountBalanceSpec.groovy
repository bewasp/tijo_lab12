package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "should return #money money for #accountNumber account number"(){

        given: "Initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)

        when: "get account balance"
        def balance = bank.accountBalance(accountNumber)

        then: "check balance"
        balance == money

        where:
        accountNumber | money
        1   | 100
        2   | 200
        3   | -300
        4   | 400
    }
}
