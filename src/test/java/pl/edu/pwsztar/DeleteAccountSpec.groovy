package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification{

    @Unroll
    def "should delete account number #accountNumber with money #accountNumberMoney"(){

        given: "initial data"
        def bank = new Bank()
        def accounts = AccountData.accountsList
        bank.addAccounts(accounts)

        when: "account deleted"
        def money = bank.deleteAccount(accountNumber)

        then: "check deleted account money"
        money == accountNumberMoney

        where:
        accountNumber | accountNumberMoney
        1   | 100
        2   | 200
        3   | -300
        4   | 400
    }
}
